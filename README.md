# Parking lot

## ENV
ruby - 2.4.1

rails - 5.1.4

app server - puma

db - postgresql

hosted on heroku


## API description

create ticket

POST /api/v1/tickets

------------------------------------

get price

GET  /api/v1/tickets/:barcode

params:
  barcode - string

------------------------------------

get state

GET  /api/v1/tickets/:barcode/state

params:
  barcode - string 

------------------------------------

create payment

POST /api/v1/tickets/:barcode/payments

params:
 barcode - string
 source - string (one of credit_card debit_card cash)

------------------------------------

get free spaces

GET /api/v1/free-spaces
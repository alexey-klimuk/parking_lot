class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.integer :ticket_id
      t.string :source

      t.timestamps
    end

    add_index :payments, :ticket_id
  end
end

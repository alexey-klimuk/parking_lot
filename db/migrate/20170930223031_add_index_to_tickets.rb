class AddIndexToTickets < ActiveRecord::Migration[5.1]
  def change
    add_index :tickets, :barcode
  end
end

module PriceCalculator
  def self.call(from)
    ((Time.zone.now - from) / 3600.0).ceil * Settings::HOURLY_PRICE
  end
end

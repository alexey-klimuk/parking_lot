module Settings
  SOURCES = %w(credit_card debit_card cash)
  HOURLY_PRICE = 2
  PAYMENT_DELAY = 15.minutes
  CAPACITY = 54
end

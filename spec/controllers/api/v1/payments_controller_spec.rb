require 'rails_helper'

describe Api::V1::PaymentsController, type: :controller do
  describe 'POST #create' do
    let(:ticket) { create(:ticket, created_at: 75.minutes.ago) }
    let(:source) { Settings::SOURCES.sample }
    let(:params) { { params: { barcode: ticket.barcode, source: source } } }

    it 'triggers ticket pay! method' do
      expect_any_instance_of(Ticket).to receive(:pay!).with(source).once
      post :create, params
    end

    it 'returns payment date' do
      post :create, params
      expect(JSON.parse(response.body)).to eq({'paid_at' => ticket.payments.last.created_at.to_s(:db)})
    end
  end
end

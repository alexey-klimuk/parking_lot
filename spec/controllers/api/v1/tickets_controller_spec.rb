require 'rails_helper'

describe Api::V1::TicketsController, type: :controller do
  describe 'POST #create' do
    it 'creates new Ticket record' do
      expect{ post :create }.to change{ Ticket.count }.by(1)
    end

    it 'returns barcode' do
      post :create
      expect(JSON.parse(response.body)).to eq({'barcode' => Ticket.last.barcode})
    end
  end

  describe 'GET #price' do
    let(:ticket) { create(:ticket, created_at: 20.minutes.ago) }

    it 'returns barcode' do
      get :price, params: { barcode: ticket.barcode }
      expect(JSON.parse(response.body)).to eq({'price' => ticket.price})
    end
  end

  describe 'GET #state' do
    let(:ticket) { create(:ticket, created_at: 20.minutes.ago) }

    it 'returns state' do
      get :state, params: { barcode: ticket.barcode }
      expect(JSON.parse(response.body)).to eq({'state' => 'unpaid'})
    end
  end
end

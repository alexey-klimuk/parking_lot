require 'rails_helper'

describe Api::V1::ApiController, type: :controller do
  describe 'GET #free_spaces' do
    let(:tickets_count) { 3 }
    let!(:ticket) { create_list(:ticket, tickets_count, created_at: 75.minutes.ago) }

    it 'returns number of free spaces available' do
      get :free_spaces
      expect(JSON.parse(response.body)).to eq({'free_spaces' => Settings::CAPACITY - tickets_count})
    end
  end
end

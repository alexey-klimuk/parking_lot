require 'rails_helper'

describe Ticket, type: :model do
  describe 'barcode validation' do
    let(:ticket) { build(:ticket) }

    context 'with blank barcode' do
      before do
        allow(ticket).to receive(:generate_barcode) { nil }
      end

      it { expect(ticket).to_not be_valid }
    end

    context 'with present barcode' do
      it { expect(ticket).to be_valid }
    end
  end

  describe 'availability validation' do
    before do
      allow(Ticket).to receive(:free_spaces_count).and_return(0)
    end

    it 'shoild be not valid if there is no free spaces available' do
      ticket = build(:ticket)
      expect(ticket).to_not be_valid
    end
  end

  describe '#price' do
    let(:ticket) { create(:ticket, created_at: 75.minutes.ago) }

    it 'returns valid price' do
      expect(ticket.price).to eq(4)
      ticket.created_at = 25.minutes.ago
      expect(ticket.price).to eq(2)
    end

    it 'returns 0 if paid less than 15 mins ago' do
      create(:payment, ticket: ticket, created_at: 10.minutes.ago)
      expect(ticket.price).to eq(0)
    end

    it 'returns extra price if paid more than 15 mins ago' do
      create(:payment, ticket: ticket, created_at: 16.minutes.ago)
      expect(ticket.price).to eq(2)
    end
  end

  describe '#state' do
    let(:ticket) { create(:ticket, created_at: Time.zone.now - 75.minutes) }

    it 'returns :paid if paid less than 15 mins ago' do
      create(:payment, ticket: ticket, created_at: 10.minutes.ago)
      expect(ticket.state).to eq(:paid)
    end

    it 'returns:unpaid if paid more than 15 mins ago' do
      create(:payment, ticket: ticket, created_at: 16.minutes.ago)
      expect(ticket.state).to eq(:unpaid)
    end

    it 'returns :unpaid if not paid' do
      expect(ticket.state).to eq(:unpaid)
    end
  end

  describe '#pay!' do
    let(:ticket) { create(:ticket, created_at: 75.minutes.ago) }

    it 'creates Payment record' do
      expect{ ticket.pay!(Settings::SOURCES.sample) }.to change{ ticket.payments.count }.by(1)
    end

    it 'prevents double payments' do
      ticket.pay!(Settings::SOURCES.sample)
      expect{ ticket.pay!(Settings::SOURCES.sample) }.to_not change{ ticket.payments.count }
    end
  end
end

require 'rails_helper'

describe Payment, type: :model do
  describe 'source validation' do
    let(:payment) { build(:payment) }
    let(:payment_invalid_source) { build(:payment, source: '123') }

    it { expect(payment).to be_valid }
    it { expect(payment_invalid_source).to_not be_valid }
  end

  describe '#actual?' do
    it 'returns true if payment created less than 15 mins ago' do
      payment = build(:payment, created_at: 5.minutes.ago)
      expect(payment.actual?).to be_truthy
    end

    it 'returns false if payment created more than 15 mins ago' do
      payment = build(:payment, created_at: 17.minutes.ago)
      expect(payment.actual?).to be_falsey
    end
  end
end

FactoryGirl.define do
  factory :payment do
    ticket
    source { Settings::SOURCES.sample }
  end
end

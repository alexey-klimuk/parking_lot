Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      post 'tickets', to: 'tickets#create', as: :tickets

      get 'tickets/:barcode', to: 'tickets#price', as: :ticket_price

      get 'tickets/:barcode/state', to: 'tickets#state', as: :ticket_state

      post 'tickets/:barcode/payments', to: 'payments#create', as: :ticket_payments

      get 'free-spaces', to: 'api#free_spaces', as: :free_spaces
    end
  end
end

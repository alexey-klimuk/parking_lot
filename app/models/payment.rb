class Payment < ApplicationRecord
  belongs_to :ticket

  validates :source, inclusion: { in: Settings::SOURCES }

  def actual?
    created_at >= Settings::PAYMENT_DELAY.ago
  end
end

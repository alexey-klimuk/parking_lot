class Ticket < ApplicationRecord
  has_many :payments

  validates :barcode, presence: true
  validate :availability, on: :create

  before_validation :generate_barcode

  def self.free_spaces_count
    Settings::CAPACITY - Ticket.where(returned: false).count
  end

  def price
    payment = last_payment
    return 0 if payment && payment.actual?
    PriceCalculator.call(payment.try(:created_at) || created_at)
  end

  def state
    payment = last_payment
    if payment && payment.actual?
      :paid
    else
      :unpaid
    end
  end

  def pay!(source)
    payments.create!(source: source) if price > 0
  end

  def return!
    if !returned && state == :paid
      update!(returned: true)
    end
  end

  private

  def last_payment
    payments.order(created_at: :desc).first
  end

  def generate_barcode
    self.barcode = loop do
      random_token = SecureRandom.hex(8)
      break random_token unless Ticket.exists?(barcode: random_token)
    end
  end

  def availability
    if Ticket.free_spaces_count <= 0
      errors[:base] << 'There is no spaces available.'
    end
  end
end

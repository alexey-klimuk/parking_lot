module Api::V1
  class TicketPaymentSerializer < ActiveModel::Serializer
    attributes :paid_at

    def paid_at
      object.created_at.to_s(:db)
    end
  end
end

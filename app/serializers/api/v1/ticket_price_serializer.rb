module Api::V1
  class TicketPriceSerializer < ActiveModel::Serializer
    attributes :price
  end
end

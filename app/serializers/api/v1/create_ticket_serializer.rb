module Api::V1
  class CreateTicketSerializer < ActiveModel::Serializer
    attributes :barcode
  end
end

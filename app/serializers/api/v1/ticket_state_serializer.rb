module Api::V1
  class TicketStateSerializer < ActiveModel::Serializer
    attributes :state
  end
end

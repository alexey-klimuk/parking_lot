class ApplicationController < ActionController::API
  rescue_from StandardError do |e|
    render json: { message: 'Internal server error' }, status: 500
  end

  rescue_from ActiveRecord::RecordNotFound do |e|
    render json: { message: 'Record not found' }, status: 404
  end

  rescue_from ActiveRecord::RecordInvalid do |e|
    render json: { message: 'Record invalid' }, status: 400
  end
end

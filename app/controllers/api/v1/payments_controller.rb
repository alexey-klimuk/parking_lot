module Api::V1
  class PaymentsController < ApiController
    # POST /api/v1/tickets/{barcode}/payments
    def create
      ticket = Ticket.find_by!(barcode: params[:barcode])
      payment = ticket.pay!(params[:source])

      render json: payment, serializer: TicketPaymentSerializer if payment
    end
  end
end

module Api::V1
  class ApiController < ApplicationController
    def free_spaces
      render json: { free_spaces: Ticket.free_spaces_count }
    end
  end
end

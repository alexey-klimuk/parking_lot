module Api::V1
  class TicketsController < ApiController
    before_action :load_ticket, only: [:price, :state]

    # POST /api/v1/tickets
    def create
      ticket = Ticket.create!

      render json: ticket, serializer: CreateTicketSerializer
    end

    # GET /api/v1/tickets/{barcode}
    def price
      render json: @ticket, serializer: TicketPriceSerializer
    end

    # GET /api/v1/tickets/{barcode}/state
    def state
      @ticket.return!

      render json: @ticket, serializer: TicketStateSerializer
    end

    private

    def load_ticket
      @ticket = Ticket.find_by!(barcode: params[:barcode])
    end
  end
end
